<?php
/**
 * InstallSchema
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        $installer = $setup;
        $installer->startSetup();
        /*
        * Drop tables if exists
        */
        $installer->getConnection()->dropTable($installer->getTable('webpos_staff'));

            $table = $installer->getConnection()->newTable(
                $installer->getTable('webpos_staff')
            )->addColumn(
                'staff_id', Table::TYPE_INTEGER, null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Staff ID'
            )->addColumn(
                'name', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Staff Name'
            )->addColumn(
                'username', Table::TYPE_TEXT, null,
                ['nullable' => false],
                'Staff Username'
            )->addColumn(
                'password', Table::TYPE_TEXT, null,
                ['nullable' => false],
                'Staff Password'
            )->addColumn(
                'email', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Email'
            )->addColumn(
                'pos_location', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Location'
            )->addColumn(
                'role', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Role'
            )->addColumn(
                'phone', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Phone'
            )->addColumn(
                'status', Table::TYPE_SMALLINT, null,
                ['nullable' => false, 'default' => '1'],
                'Status'
            );

            $installer->getConnection()->createTable($table);
            $installer->endSetup();
    }
}