<?php
/**
 * UpgradeSchema
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Zend_Db_Exception;


class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $eavSetup;

    public function __construct(\Magento\Eav\Setup\EavSetup $eavSetup)
    {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetup;
        $installer = $setup;
        $installer->startSetup();
        echo '----UPGRADE----';
        if (version_compare($context->getVersion(), '1.2.3', '<')) {

            /*
            * Drop tables if exists
            */
            $installer->getConnection()->dropTable($installer->getTable('webpos_webpos'));
            $installer->getConnection()->dropTable($installer->getTable('webpos_staff'));
            $installer->getConnection()->dropTable($installer->getTable('webpos_session'));

            $table = $installer->getConnection()->newTable($installer->getTable('webpos_webpos')
            )->addColumn(
                'pos_id', Table::TYPE_INTEGER, null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'POS ID'
            )->addColumn(
                'name', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'POS Name'
            )->addColumn(
                'location', Table::TYPE_INTEGER, null,
                ['nullable' => false],
                'POS Location'
            )->addColumn(
                'status', Table::TYPE_SMALLINT, null,
                ['nullable' => false, 'default' => '1'],
                'Status'
            );
            $installer->getConnection()->createTable($table);

            $table = $installer->getConnection()->newTable(
                $installer->getTable('webpos_staff')
            )->addColumn(
                'staff_id', Table::TYPE_INTEGER, null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Staff ID'
            )->addColumn(
                'name', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Staff Name'
            )->addColumn(
                'username', Table::TYPE_TEXT, null,
                ['nullable' => false],
                'Staff Username'
            )->addColumn(
                'password', Table::TYPE_TEXT, null,
                ['nullable' => false],
                'Staff Password'
            )->addColumn(
                'email', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Email'
            )->addColumn(
                'pos_id', Table::TYPE_INTEGER, null,
                ['unsigned' => true, 'nullable' => false],
                'POS Id'
            )->addColumn(
                'role', Table::TYPE_INTEGER, null,
                ['nullable' => false, 'default' => 1],
                'Role'
            )->addColumn(
                'phone', Table::TYPE_TEXT, null,
                ['nullable' => false, 'default' => ''],
                'Phone'
            )->addColumn(
                'status', Table::TYPE_SMALLINT, null,
                ['nullable' => false, 'default' => '1'],
                'Status'
            )->addForeignKey(
                $installer->getTable('webpos_staff'),
                'pos_id',
                $installer->getTable('webpos_webpos'),
                'pos_id'
            );
            $installer->getConnection()->createTable($table);

            $table = $installer->getConnection()->newTable($installer->getTable('webpos_session')
            )->addColumn(
                'id', Table::TYPE_INTEGER, null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                'staff_id', Table::TYPE_INTEGER, null,
                ['unsigned' => true, 'nullable' => false],
                'Staff ID'
            )->addColumn(
                'session_id', Table::TYPE_TEXT, null,
                ['nullable' => false],
                'Session Identify'
            )->addColumn(
                'pos_id', Table::TYPE_INTEGER, null,
                ['unsigned' => true, 'nullable' => false],
                'POS ID'
            )->addForeignKey(
                $installer->getTable('webpos_session'),
                'staff_id',
                $installer->getTable('webpos_staff'),
                'staff_id'
            )->addForeignKey(
                $installer->getTable('webpos_session'),
                'pos_id',
                $installer->getTable('webpos_webpos'),
                'pos_id'
            );
            $installer->getConnection()->createTable($table);

            $installer->endSetup();
        }
        if (version_compare($context->getVersion(), '1.2.4') < 0
        ) {
            $eavSetup->removeAttribute(
                Product::ENTITY,
                'visible_on_pos');

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'visible_on_pos',
                [
                    'group' => 'General',
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'visible_on_pos',
                    'input' => 'boolean',
                    'class' => '',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '0',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
    }
}
