<?php
/**
 * Webpos
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Ui\Component\Listing\Column;


use Magento\Framework\Data\OptionSourceInterface;

class Webpos implements OptionSourceInterface
{
    private $collectionWebposFactory;
    private $options = [];

    public function __construct(
        \Magestore\Product\Model\ResourceModel\Webpos\CollectionFactory $collectionWebpos
    )
    {
        $this->collectionWebposFactory = $collectionWebpos;
    }

    public function toOptionArray()
    {
        $collectionWebpos = $this->collectionWebposFactory->create();
        foreach ($collectionWebpos as $key => $webpos) {
            array_push($this->options,
                [
                    'label' => $webpos->getName(),
                    'value' => $webpos->getId()
                ]);
        }
        return $this->options;
    }

}