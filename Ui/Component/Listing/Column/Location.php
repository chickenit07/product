<?php
/**
 * Location
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class Location implements OptionSourceInterface
{
    public function toOptionArray()
    {
//        var_dump('alo');
        return [
            ['label' => __('Primary'), 'value' => 1],
            ['label' => __('Paris'), 'value' => 2],
            ['label' => __('Hanoi'), 'value' => 3]
        ];
    }
}
