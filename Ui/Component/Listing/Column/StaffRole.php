<?php
/**
 * StaffRole
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class StaffRole implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['label' => __('Cashier'), 'value' => 1]
        ];
    }
}
