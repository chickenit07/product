<?php
/**
 * Webpos
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;


class Webpos extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Webpos $resource
     * @param ResourceModel\Webpos\Collection $resourceCollection
     */
    public function __construct(
    \Magento\Framework\Model\Context $context,
    \Magento\Framework\Registry $registry,
    \Magestore\Product\Model\ResourceModel\Webpos $resource,
    \Magestore\Product\Model\ResourceModel\Webpos\Collection $resourceCollection
)
{
    parent::__construct(
        $context,
        $registry,
        $resource,
        $resourceCollection
    );
}
}