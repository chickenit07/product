<?php


namespace Magestore\Product\Model;


use Magento\Framework\Exception\LocalizedException;
use Magestore\Product\Api\CheckoutRepositoryInterface;

class CheckoutRepository extends \Magento\Framework\App\Helper\AbstractHelper implements CheckoutRepositoryInterface
{
    /**
     * @var \Magestore\Product\Api\TokenAuthenticationInterfaceFactory
     */
    private $modelTokenAuth;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Quote\Model\QuoteFactory $quote ,
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory ,
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magestore\Product\Api\TokenAuthenticationInterfaceFactory $modelTokenAuth
     * @param \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory
     * @param \Klarna\Kp\Api\QuoteRepositoryInterface $quoteRepository
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magestore\Product\Api\TokenAuthenticationInterfaceFactory $modelTokenAuth
    )
    {
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->modelTokenAuth = $modelTokenAuth;
        parent::__construct($context);
    }

    public function placeorder($customerOrder, $cartItem, $paymentMethod, $addressInformation)
    {
        /***
         * check token is valid
         */
        if (!$this->modelTokenAuth->create()->authenticToken())
            return false;

        /***
         * get store and website ID then load Customer if possible
         */
        $store = $this->storeManager->getStore();
        $storeId = $store->getStoreId();
        $websiteId = $this->storeManager->getStore()->getWebsiteId();

        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($customerOrder->getEmail());// load customer by email address

        /***
         * if not available then create new customer
         */
        if (!$customer->getId()) {
            $customer->setStore($store)
                ->setWebsiteId($websiteId)
                ->setFirstname($customerOrder->getFirstName())
                ->setLastname($customerOrder->getLastname())
                ->setEmail($customerOrder->getEmail())
                ->setPassword($customerOrder->getEmail());
            $customer->save();
        }

        /***
         *  if already have buyerId then load customer directly
         */
        $customer = $this->customerRepository->getById($customer->getId());

        /***
         * create new quote
         */
        $quote = $this->quote->create();
        $quote->setStore($store);
        $quote->assignCustomer($customer); //Assign quote to customer

        /***
         * add items to quote
         */
        foreach ($cartItem as $item) {
            $product = $this->loadProductBySku($item->getSku());
            $product->setPrice($item->getPrice());
            try {
                $quote->addProduct(
                    $product,
                    intval($item->getQty())
                );
            } catch (LocalizedException $e) {
                echo $e;
            }
        }
        /***
         * Set Address to quote. Collect Rates and Set Shipping
         */
        $quote->setBillingAddress($addressInformation->getBillingAddress());
        $quote->setShippingAddress($addressInformation->getShippingAddress());
        $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod(
            $addressInformation->getShippingCarrierCode() . '_' . $addressInformation->getShippingMethodCode()
        );

        $quote->setInventoryProcessed(false); //decrease item stock equal to qty
        $quote->save(); //quote save

        /***
         * set Payment Method
         */
        $quote->setPaymentMethod('cashondelivery'); //payment method
        $quote->setInventoryProcessed(false); //not effect inventory
        $quote->getPayment()->importData(['method' => 'cashondelivery']);


        /**
         * Collect Totals & Save Quote
         */
        $quote->collectTotals()->save();
        /**
         * Create Order From Quote
         */
//        return true;
        try {
            $order = $this->quoteManagement->submit($quote);
            $orderId = $order->getIncrementId();
            if ($order->getEntityId()) {
                $result['success'] = $orderId;
            } else {
                $result = ['error' => 1, 'msg' => 'Cannot complete your order right now.'];
                return $result;
            }
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function loadProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }
}
