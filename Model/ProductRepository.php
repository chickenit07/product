<?php
/**
 * ProductRepository
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;


use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class ProductRepository
{
    /**
     * @var \Magento\Reports\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productsFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     */
    protected $searchResultsFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var \Magestore\Product\Api\TokenAuthenticationInterfaceFactory
     */
    private $modelTokenAuth;

    /**
     * @param CollectionProcessorInterface $collectionProcessor
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory
     * @param \Magestore\Product\Api\TokenAuthenticationInterfaceFactory $modelTokenAuth
     */
    public function __construct(
        CollectionProcessorInterface $collectionProcessor,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory,
        \Magestore\Product\Api\TokenAuthenticationInterfaceFactory $modelTokenAuth
    )
    {
        $this->modelTokenAuth = $modelTokenAuth;
        $this->collectionProcessor = $collectionProcessor;
        $this->productsFactory = $productsFactory;
        $this->productRepository = $productRepository;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    public function getListProducts(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        if (!$this->modelTokenAuth->create()->authenticToken())
            return false;
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->productsFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addAttributeToSelect('*')
                    ->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResult->setTotalCount($collection->getSize());
        $sortOrdersData = $searchCriteria->getSortOrders();

        if ($sortOrdersData) {
            foreach ($sortOrdersData as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

//        $collection = $this->productsFactory->create();
//        $collection->addAttributeToSelect('*');
//
//        $this->collectionProcessor->process($searchCriteria, $collection);
//
//        $collection->load();
//
//        $searchResult->setSearchCriteria($searchCriteria);
//        $searchResult->setItems($collection->getItems());
//        $searchResult->setTotalCount($collection->getSize());
//
//        return $searchResult;

        $searchResult->setItems($collection->getItems());
        return $searchResult;
    }
}
