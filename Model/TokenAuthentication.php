<?php


namespace Magestore\Product\Model;


use Magestore\Product\Api\TokenAuthenticationInterface;

class TokenAuthentication implements TokenAuthenticationInterface
{
    protected $request;
    /**
     * @var \Magestore\Product\Model\SessionFactory
     */
    private $modelSessionFactory;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magestore\Product\Model\SessionFactory $modelSession)
    {
        $this->request = $request;
        $this->modelSessionFactory = $modelSession;
    }

    public function authenticToken()
    {
        $token = $this->request->getHeader('Authorization');
        $session = $this->modelSessionFactory->create();

        $tokenAuth = $session
            ->getCollection()
            ->addFieldToSelect('session_id')
            ->addFieldToFilter('session_id', array('eq' => $token))
            ->getData('session_id');
        if (!$tokenAuth) {
            return false;
        }
        return true;
    }
}
