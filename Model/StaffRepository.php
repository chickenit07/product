<?php
/**
 * StaffRepository
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;


use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Product\Api\StaffRepositoryInterface;

class StaffRepository implements StaffRepositoryInterface
{

    protected $modelStaff;

    public function __construct(
        \Magestore\Product\Model\Staff $modelStaff)
    {
        $this->modelStaff = $modelStaff;
    }


    public function get($username)
    {
        $staff = $this->modelStaff;
        try {
            $staff->loadByStaffUsername($username);
        } catch (LocalizedException $e) {
            throw new NoSuchEntityException(
                __(
                    'No such entity with %fieldName = %fieldValue',
                    [
                        'fieldName' => 'username',
                        'fieldValue' => $username
                    ]
                )
            );
        }
        return $staff;
    }
}