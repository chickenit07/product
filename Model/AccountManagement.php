<?php
/**
 * AccountManagement
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;


use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\UserLockedException;

class AccountManagement
{
    protected $request;
    private $staffRepo;
    private $sessionFactory;
    private $randomNumber;
    private $dateFactory;

    /**
     * AccountManagement constructor.
     * @param \Magestore\Product\Api\StaffRepositoryInterface $staffRepository
     * @param \Magestore\Product\Model\SessionFactory $session
     * @param \Magento\Framework\Math\Random $random
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
     */

    public function __construct(
        \Magestore\Product\Api\StaffRepositoryInterface $staffRepository,
        \Magestore\Product\Model\SessionFactory $session,
        \Magento\Framework\Math\Random $random,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    )
    {
        $this->dateFactory = $dateFactory;
        $this->randomNumber = $random;
        $this->sessionFactory = $session;
        $this->staffRepo = $staffRepository;
    }

    public function authenticate($username, $password)
    {
        try {
            $staff = $this->staffRepo->get($username);
        } catch (NoSuchEntityException $e) {
            throw new  InvalidEmailOrPasswordException(
                __('Invalid Password or Username')
            );
        }
        $staffId = $staff->getId();
        if (!$this->getActiveStatus()) {
            throw new UserLockedException(__('User is not active!'));
        }
        try {
            if ($staff->authenticate($staffId, $password)) {
                $modelSession = $this->sessionFactory->create();
                $modelSession
                    ->setStaffId((int)$staffId)
                    ->setPosId((int)$staff->getPosId())
                    ->setSessionId($this->randomNumber->getRandomString(16))
                    ->setCreateAt($this->dateFactory->create()->gmtDate());
                $modelSession->save();
                return $modelSession->getSessionId();
            }
        } catch (\Exception $e) {
            echo $e;
        }
        return false;
    }
    private function getActiveStatus()
    {
        return true;
    }
}
