<?php
/**
 * Session
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model\ResourceModel;


class Session extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
//    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init($this->getTable('webpos_session'), 'id');
    }
}