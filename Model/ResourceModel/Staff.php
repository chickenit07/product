<?php
/**
 * Staff
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model\ResourceModel;


use Magento\Framework\Exception\LocalizedException;

class Staff extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init($this->getTable('webpos_staff'), 'staff_id');
    }

    public function loadByStaffUsername(\Magestore\Product\Model\Staff $staff, $username)
    {
        $connection = $this->getConnection();
        $bind = ['staff_username' => $username];
        try {
            $select = $connection->select()->from($this->getMainTable())->where(
                'username = :staff_username'
            );

            $staffId = $connection->fetchOne($select, $bind);

            if($staffId) {
                $this->load($staff, $staffId);
            } else{
                $staff->setData([]);
            }
        } catch (LocalizedException $e) {
        }
        return $this;
    }
}