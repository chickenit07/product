<?php
/**
 * Collection
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model\ResourceModel\Staff;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'staff_id';

    public function _construct()
    {
        parent::_construct();
        $this->_init('Magestore\Product\Model\Staff', 'Magestore\Product\Model\ResourceModel\Staff');
    }
}