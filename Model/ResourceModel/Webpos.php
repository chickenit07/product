<?php
/**
 * Webpos
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model\ResourceModel;


class Webpos extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init($this->getTable('webpos_webpos'), 'pos_id');
    }
}