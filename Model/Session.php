<?php
/**
 * Session
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;

use Magestore\Product\Model\ResourceModel\Session as ResourceModel;
use Magestore\Product\Api\Data\SessionInterface;
use phpDocumentor\Reflection\Types\This;

class Session extends \Magento\Framework\Model\AbstractModel implements SessionInterface
{

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Webpos $resource
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magestore\Product\Model\ResourceModel\Webpos $resource
    )
    {
        $this->_init(ResourceModel::class);
        parent::__construct(
            $context,
            $registry
        );
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setID($id)
    {
        return $this->setData(self::ID, $id);
    }
    /**
     * @param int $id
     * @return $this
     */
    public function setStaffId($id)
    {
        return $this->setData(self::STAFF_ID, $id);
        // TODO: Implement setStaffId() method.
    }
    /**
     * @param int $id
     * @return $this
     */
    public function setPosId($id)
    {
        return $this->setData(self::POS_ID, $id);
        // TODO: Implement setPosId() method.
    }
    /**
     * @param int $id
     * @return $this
     */
    public function setSessionId($id)
    {
        return $this->setData(self::SESSION_ID, $id);
        // TODO: Implement setSessionId() method.
    }

    /**
     * @return int
     */
    public function getID()
    {
        return $this->getData(self::ID);
    }
    /**
     * @return int
     */
    public function getStaffId()
    {
        return $this->getData(self::STAFF_ID);
        // TODO: Implement getStaffId() method.
    }
    /**
     * @return int
     */
    public function getPosId()
    {
        return $this->getData(self::POS_ID);
        // TODO: Implement getPosId() method.
    }
    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->getData(self::SESSION_ID);
        // TODO: Implement getSessionId() method.
    }
}
