<?php
/**
 * Staff
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Model;


use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magestore\Product\Api\Data\StaffInterface;
use phpDocumentor\Reflection\Types\This;

class Staff extends \Magento\Framework\Model\AbstractModel implements StaffInterface
{
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param ResourceModel\Staff $resource
     * @param ResourceModel\Staff\Collection $resourceCollection
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magestore\Product\Model\ResourceModel\Staff $resource,
        \Magestore\Product\Model\ResourceModel\Staff\Collection $resourceCollection,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    )
    {
        $this->_encryptor = $encryptor;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    /**
     * Authenticate customer
     *
     * @param int $staffId
     * @param string $password
     * @return bool
     * @throws InvalidEmailOrPasswordException
     * @throws \Exception
     */
    public function authenticate($staffId, $password)
    {
        $hash = $this->load($staffId)->getPassword();
        if (!$this->validatePassword($hash,$password)) {
            throw new InvalidEmailOrPasswordException(
                __('Invalid login or password.')
            );
        }
        return true;
    }

    /**
     * Load Staff by username
     *
     * @param string $staffEmail
     * @return  $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadByStaffUsername($staffEmail)
    {
        $this->_getResource()->loadByStaffUsername($this, $staffEmail);
        return $this;
    }

    /**
     * Validate password with salted hash
     *
     * @param $hash
     * @param string $password
     * @return boolean
     * @throws \Exception
     */
    public function validatePassword($hash,$password)
    {
        if (!$hash) {
            return false;
        }
        return $this->_encryptor->validateHash($password, $hash);
    }

    /***
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID,$id);
        // TODO: Implement setId() method.
    }

    public function setUsername($username)
    {
        return $this->setData(self::USERNAME,$username);
        // TODO: Implement setUsername() method.
    }

    public function setPassword($password)
    {
        return $this->setData(self::PASSWORD,$password);

        // TODO: Implement setPassword() method.
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS,$status);

        // TODO: Implement setStatus() method.
    }
    /***
     * @return string
     */
    public function getUsername()
    {
        return $this->getData(self::USERNAME);
        // TODO: Implement getUsername() method.
    }
    /***
     * @return string
     */
    public function getPassword()
    {
        return $this->getData(self::PASSWORD);
        // TODO: Implement getPassword() method.
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);

        // TODO: Implement getStatus() method.
    }
}
