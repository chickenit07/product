<?php


namespace Magestore\Product\Api;


interface StaffRepositoryInterface
{

    /**
     * @param string $email
     * @return \Magestore\Product\Api\Data\StaffInterface
     */
    public function get($email);
}