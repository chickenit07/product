<?php


namespace Magestore\Product\Api;

interface CheckoutRepositoryInterface
{
    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $cartItem
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return  int Order ID.
     */
    public function placeorder($customer, array $cartItem, $paymentMethod, $addressInformation);
}
