<?php


namespace Magestore\Product\Api;


interface AccountManagementInterface
{
    /**
     * @param string $username
     * @param string $password
     * @return boolean
     */

    public function authenticate($username, $password);
}
