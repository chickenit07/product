<?php


namespace Magestore\Product\Api;


interface ProductRepositoryInterface
{
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getListProducts(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}

