<?php


namespace Magestore\Product\Api\Data;


use phpDocumentor\Reflection\Types\This;

interface SessionInterface
{
    const ID = "id";
    const STAFF_ID = "staff_id";
    const POS_ID = "pos_id";
    const SESSION_ID = "session_id";


    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @param int $id
     * @return $this
     */
    public function setStaffId($id);

    /**
     * @param int $id
     * @return $this
     */
    public function setPosId($id);

    /**
     * @param string $id
     * @return $this
     */
    public function setSessionId($id);

    /***
     * @return int
     */
    public function getId();

    /***
     * @return int
     */
    public function getStaffId();

    /***
     * @return int
     */
    public function getPosId();

    /***
     * @return string
     */
    public function getSessionId();
}
