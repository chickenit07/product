<?php


namespace Magestore\Product\Api\Data;

interface StaffInterface
{
    const ID = "staff_id";
    const USERNAME = "username";
    const PASSWORD = "password";
    const STATUS = "status";

    /**
     * @param int $staffId
     * @param string $password
     * @return boolean
     */
    public function authenticate($staffId, $password);

    /**
     * @param string $staffEmail
     * @return $this
     */
    public function loadByStaffUsername($staffEmail);

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);


    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username);


    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password);

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status);

    /***
     * @return int
     */
    public function getId();

    /***
     * @return string
     */
    public function getUsername();

    /***
     * @return string
     */
    public function getPassword();

    /***
     * @return boolean
     */
    public function getStatus();
}
