<?php


namespace Magestore\Product\Api;


interface TokenAuthenticationInterface
{
    /***
     * @return bool
     */
    public function authenticToken();
}
