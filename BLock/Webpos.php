<?php
/**
 * Webpos
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\BLock;


class Webpos extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @var \Magestore\Product\Model\ResourceModel\Webpos
     */
    protected $_resourceModel;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\User\Model\ResourceModel\User $resourceModel
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Widget\Context $context,
    \Magento\User\Model\ResourceModel\User $resourceModel,
    array $data = []
) {
    parent::__construct($context, $data);
    $this->_resourceModel = $resourceModel;
}

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
{
    $this->addData(
        [
            \Magento\Backend\Block\Widget\Container::PARAM_CONTROLLER => 'webpos',
            \Magento\Backend\Block\Widget\Grid\Container::PARAM_BLOCK_GROUP => 'Magestore_Webpos',
            \Magento\Backend\Block\Widget\Grid\Container::PARAM_BUTTON_NEW => __('Add POS'),
            \Magento\Backend\Block\Widget\Container::PARAM_HEADER_TEXT => __('WEBPOS'),
        ]
    );
    parent::_construct();
    $this->_addNewButton();
}
}
