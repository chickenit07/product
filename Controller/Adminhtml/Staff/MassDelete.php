<?php
/**
 * massDelete
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Controller\Adminhtml\Staff;


use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magestore\Product\Model\ResourceModel\Staff\Collection;
use Magestore\Product\Model\ResourceModel\Staff\CollectionFactory;

/**
 * Class MassDelete
 * @package Magestore\Product\Controller\Adminhtml\Staff
 */
class MassDelete extends AbstractMassAction
{
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    )
    {
        parent::__construct($context, $filter, $collectionFactory);
    }

    /**
     * @param Collection $collection
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function massAction(Collection $collection)
    {
        //TODO
        $count = 0;
        foreach ($collection as $staffModel) {
            $staffModel->delete();
            $count++;
        }
        $this->messageManager->addSuccess('You have deleted %1 staff', $count);
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;
    }
}
