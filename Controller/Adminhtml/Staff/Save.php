<?php
/**
 * Save
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Controller\Adminhtml\Staff;

use phpDocumentor\Reflection\Types\This;

class Save extends \Magestore\Product\Controller\Adminhtml\Staff
{
    protected $modelStaffFactory;
    protected $encryptor;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Encryption\EncryptorInterface  $encryptor,
        \Magestore\Product\Model\StaffFactory $modelStaffFactory)
    {
        $this->encryptor = $encryptor;
        $this->modelStaffFactory = $modelStaffFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        //$resultRedirect = $this->resultRedirectFactory->create();
        $staffId = (int)$this->getRequest()->getParam('staff_id');
        $staffPassword = $this->getRequest()->getParam('password');
        $hashPassword = $this->encryptor->hash($staffPassword);

        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $date = time();
            if ($staffId) {
                $staff_model = $this->modelStaffFactory->create()->load($staffId);
            } else {
                $staff_model = $this->modelStaffFactory->create();
                $staff_model->setData('created_at', $date);
            }
            $staff_model->setData($data);
            $staff_model->setData('updated_at', $date);
            $staff_model->setData('password',$hashPassword);
            try {
                $staff_model->save();
                $this->messageManager->addSuccess(__('Staff was successfully saved'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('staff_id')]);
            }
            if ($this->getRequest()->getParam('back') == 'edit') {
                return $resultRedirect->setPath('*/*/edit', ['id' => $staff_model->getId()]);
            }
            return $resultRedirect->setPath('*/*/');
        }
    }
}
