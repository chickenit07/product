<?php
/**
 * Edit
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Controller\Adminhtml\Webpos;

/***
 * Class Edit
 * @package Magestore\Product\Controller\Adminhtml\Staff
 */
class Edit extends \Magestore\Product\Controller\Adminhtml\Webpos
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    protected $modelWebpos;
    protected $registry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magestore\Product\Model\WebposFactory $modelStaff
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magestore\Product\Model\WebposFactory $modelWebpos,
        \Magento\Framework\Registry $registry
    )
    {
        $this->registry = $registry;
        $this->modelWebpos = $modelWebpos;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->modelWebpos->create();
        if ($id) {
            $model = $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This POS no longer exists.'));
                return $resultRedirect->setPath('webpos/*/', ['_current' => true]);
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->registry->register('current_pos', $model);

        $resultPage = $this->_resultPageFactory->create();

        if (!$model->getId()) {
            $pageTitle = __('New POS');
        } else {
            $pageTitle = __('Edit POS %1', $model->getName());
        }

        $resultPage->getConfig()->getTitle()->prepend($pageTitle);
        return $resultPage;
    }
}
