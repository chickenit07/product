<?php
/**
 * NewAction
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Controller\Adminhtml\Webpos;


use Magento\Framework\Controller\ResultFactory;

class NewAction extends \Magestore\Product\Controller\Adminhtml\Webpos
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        return $resultForward->forward('edit');
    }
}
