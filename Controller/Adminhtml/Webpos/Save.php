<?php
/**
 * Save
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Controller\Adminhtml\Webpos;

use phpDocumentor\Reflection\Types\This;

class Save extends \Magestore\Product\Controller\Adminhtml\Webpos
{
    protected $modelWebposFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Encryption\EncryptorInterface  $encryptor,
        \Magestore\Product\Model\WebposFactory $modelWebposFactory)
    {
        $this->modelWebposFactory = $modelWebposFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        //$resultRedirect = $this->resultRedirectFactory->create();
        $posId = (int)$this->getRequest()->getParam('pos_id');

        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $date = time();
            if ($posId) {
                $pos_model = $this->modelWebposFactory->create()->load($posId);
            } else {
                $pos_model = $this->modelWebposFactory->create();
                $pos_model->setData('created_at', $date);
            }
            $pos_model->setData($data);
            $pos_model->setData('updated_at', $date);
            try {
                $pos_model->save();
                $this->messageManager->addSuccess(__('POS was successfully saved'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('pos_id')]);
            }
            if ($this->getRequest()->getParam('back') == 'edit') {
                return $resultRedirect->setPath('*/*/edit', ['id' => $pos_model->getId()]);
            }
            return $resultRedirect->setPath('*/*/');
        }
    }
}
