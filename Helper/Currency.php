<?php
/**
 * Currency
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Helper;


class Currency
{
    protected $currencyFactory;
    protected $storeManager;
    protected $tokenAuthentication;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magestore\Product\Api\TokenAuthenticationInterfaceFactory $tokenAuthentication
    )
    {
        $this->tokenAuthentication = $tokenAuthentication;
        $this->storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrencySymbol()
    {
        if (!$this->tokenAuthentication->create()->authenticToken()) return false;
        $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();
        return $this->currencyFactory->create()->load($currencyCode)->getCurrencySymbol();
    }
}
